Overview
========

.. {# pkglts, glabpkg_dev

.. image:: https://b326.gitlab.io/allen1989/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/allen1989/0.2.0/


.. image:: https://b326.gitlab.io/allen1989/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/allen1989


.. image:: https://b326.gitlab.io/allen1989/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://b326.gitlab.io/allen1989/


.. image:: https://badge.fury.io/py/allen1989.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/allen1989




main: |main_build|_ |main_coverage|_

.. |main_build| image:: https://gitlab.com/b326/allen1989/badges/main/pipeline.svg
.. _main_build: https://gitlab.com/b326/allen1989/commits/main

.. |main_coverage| image:: https://gitlab.com/b326/allen1989/badges/main/coverage.svg
.. _main_coverage: https://gitlab.com/b326/allen1989/commits/main
.. #}

Formalisms from Allen at al. 1989 paper
