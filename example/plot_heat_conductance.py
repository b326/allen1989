"""
Heat conductance canopy atmosphere
==================================

Plot heat conductance between canopy (or soil) and atmosphere as a function
of wind speed.
"""
import matplotlib.pyplot as plt
import numpy as np

from allen1989.transfer import heat_conductance

# compute data
data = []
wss = np.linspace(0., 10, 100)

for hc in (1.5, 1, 0.5):
    data.append((hc, [heat_conductance(hc, ws) for ws in wss]))

# plot curves
fig, axes = plt.subplots(1, 1, figsize=(10, 6), squeeze=False)
ax = axes[0, 0]

for hc, conds in data:
    ax.plot(wss, conds, label=f"{hc:.1f}")

ax.legend(loc='upper left', title="canopy height [m]")
ax.set_xlabel("ws [m.s-1]")
ax.set_ylabel("ga [m.s-1]")

fig.tight_layout()
plt.show()
